import pandas as pd
from pandas.api.types import is_float_dtype
from pandas.testing import assert_frame_equal

from transformers_testing_for_data_science import ToNumericNans_Right
  
def test_to_numeric_nans_transformer_fixture(get_df, get_df_expected):
    to_numeric_nans = ToNumericNans_Right()
    df = get_df
    null_columns = df.columns[df.isnull().all()].tolist()
    
    df_transformed = to_numeric_nans.fit_transform(df)
    
    for i in null_columns:
        assert is_float_dtype(df_transformed.loc[:, i])
    
    assert_frame_equal(df_transformed, get_df_expected, check_dtype=True)