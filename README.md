# Testing for Data Science

# Описание
Код к семинару по тестированию в Data Science.

Слайды можно посмотреть в папке slides.

# Description
Examples for Tutorial for Tesing in Data Science.

Slides available in folder slides.

# Запуск проекта
1. Создайте виртуальное окружение любым удобным способом: virtualenv, venv, pipenv. Версия Python 3.6.
2. Активируйте виртуальное окружение
~~~~{.python}
source [venv_name]/bin/activate
~~~~
3. Установите нужные библиотеки
~~~~{.python}
pip install -r requirements.txt
~~~~
4. Добавить отображение виртуального окружения в Jupyter.
~~~~{.python}
python -m ipykernel install --user --name Testing_Py36
~~~~
5. Открыть папку notebooks и запускать их по порядку.

# Installation

1. Create Virtual Environment with your favourite tool: virtualenv, venv, pipenv. Python version 3.6.
2. Аctivate Virtual Environment
~~~~{.python}
source [venv_name]/bin/activate
~~~~
3. Install libraries
~~~~{.python}
pip install -r requirements.txt
~~~~
4. Add Venv to Jupyter.
~~~~{.python}
python -m ipykernel install --user --name [venv_name]
~~~~
5. Open folder notebooks and follow the guideline.

# Видео

[Лекция на русском](https://www.youtube.com/watch?v=u5NxhpjW4To)